module.exports = function service(options) {

  this.add({ type: 'ETL', function: 'collares' }, collares)
  this.add({ type: 'calculadora', function: 'restar' }, restar)
  this.add({ type: 'calculadora', function: 'multiplicar' }, multiplicar)
  this.add({ type: 'calculadora', function: 'dividir' }, dividir)

  function  collares(msg, respond) {
    var vars = msg.vars;
    var num = 0;
    for(var i=0; i<vars.length; i++){
    var num = parseInt(vars[i], 10)+num;
    }
    respond( null, { respond: num});
  }

  function restar(msg, respond) {
      var vars = msg.vars;
      var num = 0;
      for(var i=0; i<vars.length; i++){
      var num = parseInt(vars[i], 10)-num;
      }
      //var num2 = vars.num2;
      respond( null, { respond: num});
  }

  function multiplicar(msg, respond) {
      var vars = msg.vars;
      var num = 1;
      for(var i=0; i<vars.length; i++){
      var num = parseInt(vars[i], 10)*num;
      }
      //var num2 = vars.num2;
      respond( null, { respond: num});
  }

  function dividir(msg, respond) {
      var vars = msg.vars;
      var num = 1;
      for(var i=0; i<vars.length; i++){
      var num = parseInt(vars[i], 10)/num;
      }
      //var num2 = vars.num2;
      respond( null, { respond: num});
  }

}
