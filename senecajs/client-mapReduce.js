var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var url = 'mongodb://localhost:27017/test';
var col = 'eventos';

  MongoClient.connect(url, function(err, db){
      assert.equal(null, err);
      console.log("Conectado a MongoDB");

      console.time('MapReduce');
      allez(db,function(){})
  })

var allez = function(db,callback) {

    db.collection(col).mapReduce(
     function() { emit(this.pulsaciones,this.temperatura); },

     function(key, values) {return values.length}, {
        query:{pulsaciones: {$gt: 190} },
        out:"post_total"
     }
    )
    console.timeEnd('MapReduce');
    callback();
  };
